﻿using BIZ.Managers;
using COMMON.validadores;
using COMMON.Validadores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BIZ
{
	public static class FabricManager
	{
		public static CitasManager CitasManager() => new CitasManager(new CitasValidator());
		public static EmpleadoManager EmpleadoManager()=>new EmpleadoManager(new EmpleadoValidator());
		public static ProductosManager ProductosManager()=> new ProductosManager(new ProductosValidator());
		public static ServiciosManager ServiciosManager()=> new ServiciosManager(new ServiciosValidator());
		public static VentasManager VentasManager() => new VentasManager(new VentasValidator());
	}
}
