﻿using COMMON.Entidades;
using COMMON.Validadores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BIZ.Managers
{
	public class CitasManager:GenericManager<Citas>
	{
		public CitasManager(GenericValidator<Citas> validator):base(validator)
		{

		}
	}
}
