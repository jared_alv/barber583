﻿using COMMON.Entidades;
using COMMON.Validadores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BIZ.Managers
{
	public class EmpleadoManager : GenericManager<Empleado>
	{
		public EmpleadoManager(GenericValidator<Empleado> validator) : base(validator)
		{
		}
	}
}
