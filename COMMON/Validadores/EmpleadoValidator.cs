﻿using COMMON.Validadores;
using COMMON.Entidades;
using FluentValidation;

namespace COMMON.validadores
{
	public class EmpleadoValidator:GenericValidator<Empleado>
	{
		public EmpleadoValidator()
		{
			RuleFor(x => x.nombre_empleado).NotEmpty().MaximumLength(50);
		//Faltan mas
		}
	}
}
