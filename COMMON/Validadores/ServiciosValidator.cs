﻿using COMMON.Entidades;
using FluentValidation;

namespace COMMON.Validadores
{
    public class ServiciosValidator:GenericValidator<Servicios>
    {
        public ServiciosValidator()
        {
            RuleFor(x => x.nombre_servicio).NotEmpty().MinimumLength(5).MaximumLength(50);
            RuleFor(x => x.precio).NotEmpty().GreaterThan(0);
        }
    }
}
