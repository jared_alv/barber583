﻿namespace COMMON.Entidades
{
    public class Ventas:Base
    {
        public string id_empleado { get; set; }
        public string id_producto { get; set; }
        public string id_servicios { get; set; }
        public string nombre_empleado { get; set; }
        public string producto { get; set; }
        public int cantidad { get; set; }
        public DateTime fecha_venta { get; set; }
        public decimal precio { get; set; }
        public string nombre_servicio { get; set; }
    }
}
