﻿namespace COMMON.Entidades
{
    public class Productos:Base
    {
        public string nombreProducto { get; set; }
        public int cantidad { get; set; }
        public decimal precio { get; set; }
    }
}
