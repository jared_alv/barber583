﻿namespace COMMON.Entidades
{
    public class Citas:Base
    {
        public string id_empleado { get; set; }
        public string id_servicios { get; set; }
        public DateTime fecha_cita { get; set; }
        public string nombre_empleado { get; set; }
        public string nombre_servicio { get; set; }

    }
}
