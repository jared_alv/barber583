﻿namespace COMMON.Entidades
{
	public class Empleado:Base
	{
		public string nombre_empleado { get; set; }
		public string apellido_empelado { get; set; }
		public string curp { get; set; }
		public string telefono { get; set; }
		public string correo_electronico { get; set; }
		public string fecha_nacimiento { get; set; }
	}
}
