﻿using COMMON.Entidades;
using FluentValidation;

namespace COMMON.Validadores
{
    public class VentasValidator:GenericValidator<Ventas>
    {
        public VentasValidator()
        {
            RuleFor(x => x.nombre_empleado).NotEmpty().MaximumLength(1).MinimumLength(50);
            RuleFor(x => x.producto).NotEmpty().MaximumLength(1).MinimumLength(50);
            RuleFor(x => x.cantidad).NotEmpty().GreaterThan(0);
            RuleFor(x => x.fecha_venta).NotEmpty();
            RuleFor(x => x.precio).NotEmpty().GreaterThan(0);
            RuleFor(x => x.nombre_servicio).NotEmpty().MaximumLength(1).MinimumLength(50);


        }
    }
}
