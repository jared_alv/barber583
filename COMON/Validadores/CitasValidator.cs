﻿using COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Validadores
{
	public class CitasValidator:GenericValidator<Citas>
	{
		public CitasValidator()
		{
			RuleFor(c=>c.id_empleado).NotEmpty();
			RuleFor(c=>c.id_servicios).NotEmpty();
			RuleFor(c=>c.fecha_cita).NotEmpty();
			

		}
	}
}
