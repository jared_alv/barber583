﻿using COMMON.Entidades;
using FluentValidation;
using System.Data;

namespace COMMON.Validadores
{
    public abstract class GenericValidator<T> : AbstractValidator<T> where T: Base
    {
        public GenericValidator()    
        {
            RuleFor(x => x.Id).NotEmpty();

        }
    }
}
