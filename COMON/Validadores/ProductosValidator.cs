﻿using COMMON.Entidades;
using FluentValidation;

namespace COMMON.Validadores
{
    public class ProductosValidator:GenericValidator<Productos>
    {
        public ProductosValidator()
        {
            RuleFor(x => x.nombreProducto).NotEmpty().MinimumLength(1).MaximumLength(50);
            RuleFor(x => x.cantidad).NotEmpty().GreaterThan(0);
            RuleFor(x => x.precio).NotEmpty().GreaterThan(0);
        }
    }
}
