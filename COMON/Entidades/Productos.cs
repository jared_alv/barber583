﻿namespace COMMON.Entidades
{
    public class Productos:Base
    {
        public string nombreProducto { get; set; }
        public int cantidad { get; set; }
        public double precio { get; set; }

		public override string ToString()
		{
			return $"{nombreProducto} - {cantidad} - {precio}";
		}
	}
}
