﻿namespace COMMON.Entidades
{
    public class Servicios:Base
    {
        public string nombre_servicio { get; set; }
        public decimal precio { get; set; }
		public override string ToString()
		{
			return $"{nombre_servicio} - {precio}";
		}
	}
}
