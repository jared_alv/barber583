﻿using COMMON.Entidades;
using COMMON.Validadores;
using LiteDB;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Barberia.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public abstract class GenericController<T>: ControllerBase where T :Base
    {
        protected string DBName = "RegistroBarber.db";
        private GenericValidator<T> validator;

        public GenericController(GenericValidator<T> validador)
        {

            validator = validador;
        }
        // GET: api/<GenericControllerController>
        [HttpGet]
        public ActionResult<List<T>> Get()
        {
            try
            {
                List<T> datos = new List<T>();
                using (var db = new LiteDatabase(DBName)) //Cadena de conexion
                {
                    //typeof: Regresa el tipo de T, que en este caso sería el nombre de una tabla
                    var col = db.GetCollection<T>(typeof(T).Name);
                    datos = col.FindAll().ToList();
                }
                return Ok(datos); //Si se inserta de manera correcta manda un ok
            }
            catch (Exception)
            {

                return BadRequest(null); //Si no se inserta regresa una mala peticion
            }
        }

        // GET api/<GenericControllerController>/5
        [HttpGet("{id}")]
        public ActionResult<T> Get(string id)
        {
            try
            {
                T dato;
                using (var db = new LiteDatabase(DBName)) //Cadena de conexion
                {
                    var col = db.GetCollection<T>(typeof(T).Name);
                    dato = col.FindById(id);
                }
                return Ok(dato); //Si se inserta de manera correcta manda un ok
            }
            catch (Exception)
            {

                return BadRequest(null); //Si no se inserta regresa una mala peticion
            }
        }

        // POST api/<GenericControllerController>
        [HttpPost]
        public ActionResult<bool> Post([FromBody] T value)
        {
            try
            {
                //Permite editar 
                value.Id = Guid.NewGuid().ToString();
                //Quiero que me valide lo que trae, osea la T
                var resultadoValidacion = validator.Validate(value);
                //Si es valido inserta, si no regresa todos los errores
                if (resultadoValidacion.IsValid)
                {


                    using (var db = new LiteDatabase(DBName)) //Cadena de conexion
                    {
                        var col = db.GetCollection<T>(typeof(T).Name);
                        col.Insert(value);//Iserta el registro
                    }
                    return Ok(true); //Si se inserta de manera correcta manda un ok
                }
                else
                {

                    //Aquí va a guardar todos los errores que contenga, por cada campo
                    string error = "";
                    foreach (var item in resultadoValidacion.Errors)
                    {
                        error += item.ErrorMessage + ". ";
                    }
                    return BadRequest(error);
                }
            }
            catch (Exception)
            {

                return BadRequest(false); //Si no se inserta regresa una mala peticion
            }
            //Insertat un registro
        }

        // PUT api/<GenericControllerController>/5
        [HttpPut("{id}")]
        public ActionResult<T> Put(int id, [FromBody] T value)
        {
            try
            {
                var resultadoValidacion = validator.Validate(value);
                if (resultadoValidacion.IsValid)
                {
                    using (var db = new LiteDatabase(DBName)) //Cadena de conexion
                    {
                        var col = db.GetCollection<T>(typeof(T).Name);
                        col.Update(id, value);
                    }
                    return Ok(true); //Si se inserta de manera correcta manda un ok
                }
                else
                {
                    string error = "";
                    foreach (var item in resultadoValidacion.Errors)
                    {
                        error += item.ErrorMessage + ". ";
                    }
                    return BadRequest(error);

                }
            }
            catch (Exception)
            {

                return BadRequest(false); //Si no se inserta regresa una mala peticion
            }
        }

        // DELETE api/<GenericControllerController>/5
        [HttpDelete("{id}")]
        public ActionResult<bool> Delete(string id)
        {
            try
            {

                using (var db = new LiteDatabase(DBName)) //Cadena de conexion
                {
                    var col = db.GetCollection<T>(typeof(T).Name);
                    col.Delete(id);
                }
                return Ok(true); //Si se inserta de manera correcta manda un ok
            }
            catch (Exception)
            {

                return BadRequest(false); //Si no se inserta regresa una mala peticion
            }
        }
        //Prueba 1
    }
}
