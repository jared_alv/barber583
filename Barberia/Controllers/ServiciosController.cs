﻿using COMMON.Entidades;
using COMMON.Validadores;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Barberia.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ServiciosController : GenericController<Servicios>
    {
        public ServiciosController():base ( new ServiciosValidator())
        {

        }
    }
}
