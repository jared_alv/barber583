﻿
using COMMON.Entidades;
using COMMON.validadores;
using COMMON.Validadores;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Barberia.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class EmpleadoController : GenericController<Empleado>
	{
		public EmpleadoController():base(new EmpleadoValidator())
		{

		}
	}
}
