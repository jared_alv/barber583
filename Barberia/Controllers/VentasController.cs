﻿using COMMON.Entidades;
using COMMON.Validadores;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Barberia.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VentasController : GenericController<Ventas>
    {
        public VentasController():base( new VentasValidator())
        {

        }
    }
    
}
