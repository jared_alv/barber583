﻿using COMMON.Entidades;
using COMMON.Validadores;

namespace Barberia.Controllers
{
	public class CitasController : GenericController<Citas>
	{
		public CitasController() : base(new CitasValidator())
		{
		}
	}
}
