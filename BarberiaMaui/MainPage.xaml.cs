﻿using BarberiaMaui.Pages;

namespace BarberiaMaui
{
	public partial class MainPage : ContentPage
	{
		int count = 0;

		public MainPage()
		{
			InitializeComponent();
		}
		private void Button_Clicked(object sender, EventArgs e)
		{
			Navigation.PushAsync(new CatalogoCitas());
		}
		private void Button_Clicked_3(object sender, EventArgs e)
		{
			Navigation.PushAsync(new CatalogoEmpleado());
        }

		private void Button_Clicked_4(object sender, EventArgs e)
		{
			Navigation.PushAsync(new CatalogoProductos());
        }

		private void btnServicios_Clicked(object sender, EventArgs e)
		{
			Navigation.PushAsync(new CatalogoServicios());
        }
    }
}