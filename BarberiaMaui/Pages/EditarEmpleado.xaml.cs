using BIZ;
using BIZ.Managers;
using COMMON.Entidades;

namespace BarberiaMaui.Pages;

public partial class EditarEmpleado : ContentPage
{
	Empleado empleado;
	EmpleadoManager empleadoManager;
	bool esNuevo;
	public EditarEmpleado(Empleado empleado)
	{
		InitializeComponent();
		this.BindingContext= empleado;
		this.empleado=empleado;
		esNuevo = string.IsNullOrEmpty(empleado.Id);
		empleadoManager = FabricManager.EmpleadoManager();
		//List<Empleado> empleado = empleadoManager.ObtenerTodos;
		btnEliminar.IsVisible = !esNuevo;
	}

	private void btnGuardar_Clicked(object sender, EventArgs e)
	{
     Empleado u = this.BindingContext as Empleado;
	 if (u.password == entContrase�a2.Text) {
			Empleado result = esNuevo ? empleadoManager.Insertar(u) : empleadoManager.Modificar(u, u.Id);
			if (result != null)
			{
				DisplayAlert("�xito", "Datos guardados correctamente", "Ok");
				Navigation.PopAsync();
			}
			else
			{
				DisplayAlert("Error", empleadoManager.Error, "Ok");
			}
		}
		else
		{
			DisplayAlert("Error", "Las contrase�as no son iguales", "Ok");
		}
	
	}

	private async void btnEliminar_Clicked(object sender, EventArgs e)
	{
	try
	{
		var r = await DisplayAlert("Confirma", "�Estas seguro de eliminar este registro?", "Si", "No");
			if (r)
			{
				if (empleadoManager.Eliminar(empleado.Id))
				{
					await DisplayAlert("�xito", "Registro eliminado correctamente", "Ok");
					await Navigation.PopAsync();
				}
				else
				{
					await DisplayAlert("Error", empleadoManager.Error, "Ok");
				}
			}
	}
	catch (Exception ex)
	{
			await DisplayAlert("Error", ex.Message, "Ok");
		}
	}
}
