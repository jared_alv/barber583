using BIZ;
using BIZ.Managers;
using COMMON.Entidades;

namespace BarberiaMaui.Pages;

public partial class CatalogoEmpleado : ContentPage
{
	EmpleadoManager empleadoManager;

	List<Empleado> empleados;

	public CatalogoEmpleado()
	{
		InitializeComponent();
		empleadoManager = FabricManager.EmpleadoManager();
		empleados = FabricManager.EmpleadoManager().ObtenerTodos;
	}
	protected override void OnAppearing()
	{
		base.OnAppearing();
		lstEmpleado.ItemsSource = null;
		lstEmpleado.ItemsSource = empleadoManager.ObtenerTodos;
	}

	private void lstEmpleado_ItemSelected(object sender, SelectedItemChangedEventArgs e)
	{
		Navigation.PushAsync(new EditarEmpleado(e.SelectedItem as Empleado));
	}

	private void Button_Clicked(object sender, EventArgs e)
	{
		Navigation.PushAsync(new EditarEmpleado(new COMMON.Entidades.Empleado()));
	}
}