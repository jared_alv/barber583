using BIZ;
using BIZ.Managers;
using COMMON.Entidades;

namespace BarberiaMaui.Pages;

public partial class EditarProductos : ContentPage
{
	Productos productos;
	ProductosManager productosManager;
	bool esNuevo;

	public EditarProductos(Productos productos)
	{
		InitializeComponent();
		this.BindingContext = productos;
		this.productos = productos;
		esNuevo = string.IsNullOrEmpty(productos.Id);
		productosManager = FabricManager.ProductosManager();
		//List<Empleado> empleado = empleadoManager.ObtenerTodos;
		btnEliminar.IsVisible = !esNuevo;
	}

	private void btnGuardar_Clicked(object sender, EventArgs e)
	{
		Productos c = this.BindingContext as Productos;
		
		
			Productos result = esNuevo ? productosManager.Insertar(c) : productosManager.Modificar(c, c.Id);
			if (result != null)
			{
				DisplayAlert("�xito", "Datos guardados correctamente", "Ok");
				Navigation.PopAsync();
			}
			else
			{
				DisplayAlert("Error", productosManager.Error, "Ok");
			}
		}
		

	private async void btnEliminar_Clicked(object sender, EventArgs e)
	{
		try
		{
			var r = await DisplayAlert("Confirma", "�Estas seguro de eliminar este registro?", "Si", "No");
			if (r)
			{
				if (productosManager.Eliminar(productos.Id))
				{
					await DisplayAlert("�xito", "Registro eliminado correctamente", "Ok");
					await Navigation.PopAsync();
				}
				else
				{
					await DisplayAlert("Error", productosManager.Error, "Ok");
				}
				
			}
		}
		catch (Exception ex)
		{

			await DisplayAlert("Error", ex.Message, "Ok");
		}
	}
}
