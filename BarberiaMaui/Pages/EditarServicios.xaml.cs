using BIZ;
using BIZ.Managers;
using COMMON.Entidades;

namespace BarberiaMaui.Pages;

public partial class EditarServicios : ContentPage
{
	Servicios servicios;
	ServiciosManager serviciosManager;
	bool esNuevo;
	public EditarServicios(Servicios servicios)
	{
		InitializeComponent();
		this.BindingContext = servicios;
		this.servicios = servicios;
		esNuevo = string.IsNullOrEmpty(servicios.Id);
		serviciosManager = FabricManager.ServiciosManager();
		btnEliminar.IsVisible = !esNuevo;

	}

	private void btnGuardar_Clicked(object sender, EventArgs e)
	{
		Servicios u=this.BindingContext as Servicios;

		Servicios result = esNuevo ? serviciosManager.Insertar(u) : serviciosManager.Modificar(u, u.Id);
		if (result != null)
		{
			DisplayAlert("�xito","Datos Guardados Correctamente","OK");
			Navigation.PopAsync();
		}
		else
		{
			DisplayAlert("Error", serviciosManager.Error, "OK");
		}
    }

	private async void btnEliminar_Clicked(object sender, EventArgs e)
	{
		try
		{
			var r = await DisplayAlert("Confirma", "�Estas seguro de eliminar este registro?", "Si", "No");
			if (r)
			{
				if (serviciosManager.Eliminar(servicios.Id))
				{
					await DisplayAlert("�xito", "Registro eliminado correctamente", "Ok");
					await Navigation.PopAsync();
				}
				else
				{
					await DisplayAlert("Error", serviciosManager.Error, "Ok");
				}
			}
		}
		catch (Exception ex)
		{

			await DisplayAlert("Error", ex.Message, "Ok");
		}
    }
}