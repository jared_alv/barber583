using BIZ;
using BIZ.Managers;
using COMMON.Entidades;

namespace BarberiaMaui.Pages;

public partial class EditarCitas : ContentPage
{
	Citas citas;
	CitasManager citasManager;
	ServiciosManager serviciosManager;
	EmpleadoManager empleadoManager;
	bool esNuevo;


	public EditarCitas(Citas citas)
	{
		InitializeComponent();
		this.BindingContext= citas;
		this.citas = citas;
		esNuevo = string.IsNullOrEmpty(citas.Id);
		citasManager = FabricManager.CitasManager();
		serviciosManager = FabricManager.ServiciosManager();
		empleadoManager=FabricManager.EmpleadoManager();
		List<Servicios> servicios = serviciosManager.ObtenerTodos;
		pkrServicio.ItemsSource = servicios;
		List<Empleado> empleados = empleadoManager.ObtenerTodos;
		pkrEmpleado.ItemsSource = empleados;
		btnEliminar.IsVisible =!esNuevo;

		if (!esNuevo)
		{
			pkrServicio.SelectedItem = servicios.Where(c => c.Id == citas.id_servicios).SingleOrDefault();
			pkrEmpleado.SelectedItem = empleados.Where(d => d.Id == citas.id_empleado).SingleOrDefault();
		}

	}

	private void btnGuardar_Clicked(object sender, EventArgs e)
	{
		Citas c = this.BindingContext as Citas;
		Empleado u = pkrEmpleado.SelectedItem as Empleado;	
		Servicios r = pkrServicio.SelectedItem as Servicios;
		c.id_empleado = u.Id;
		c.id_servicios = r.Id;
		c.fecha_cita = pkrFecha.Date;

		Citas result = esNuevo ? citasManager.Insertar(c) : citasManager.Modificar(c, c.Id);
		if (result != null)
		{
			DisplayAlert("�xito", "Datos guardados correctamente", "Ok");
			Navigation.PopAsync();
		}
		else
		{
			DisplayAlert("Error", citasManager.Error, "Ok");
		}
	}

    

	private async void btnEliminar_Clicked(object sender, EventArgs e)
	{
		try
		{
			var r = await DisplayAlert("Confrima", "�Estas Seguro de eliminar este registro?", "SI", "NO");
			if (r)
			{
				if (citasManager.Eliminar(citas.Id))

				{
					await DisplayAlert("�xito", "Registros eliminado correcta", "OK");
					await Navigation.PopAsync();
				}
				else
				{
					await DisplayAlert("Error", citasManager.Error, "OK");

				}
			}
		}
		catch (Exception ex)
		{

			await DisplayAlert("Error", ex.Message, "OK");
		}
    }
}