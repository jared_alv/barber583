using BIZ;
using BIZ.Managers;
using COMMON.Entidades;

namespace BarberiaMaui.Pages;

public partial class CatalogoProductos : ContentPage
{
	ProductosManager productosManager;
	List<Productos> productos;
	public CatalogoProductos()
	{
		InitializeComponent();
		productosManager = FabricManager.ProductosManager();
		productos = FabricManager.ProductosManager().ObtenerTodos;
	}
	protected override void OnAppearing()
	{
		base.OnAppearing();
		lstProductos.ItemsSource = null;
		lstProductos.ItemsSource = productosManager.ObtenerTodos;
	}

	private void lstProductos_ItemSelected(object sender, SelectedItemChangedEventArgs e)
	{
		Navigation.PushAsync(new EditarProductos(e.SelectedItem as Productos));
	}

	private void Button_Clicked(object sender, EventArgs e)
	{
		Navigation.PushAsync(new EditarProductos(new COMMON.Entidades.Productos()));
	}
}