using BIZ;
using BIZ.Managers;
using COMMON.Entidades;

namespace BarberiaMaui.Pages;

public partial class CatalogoCitas : ContentPage
{
	CitasManager citasManager;

	List<Citas> citas;
	public CatalogoCitas()
	{
		InitializeComponent();
		citasManager = FabricManager.CitasManager();
		citas = FabricManager.CitasManager().ObtenerTodos;

	}

	protected override void OnAppearing()
	{
		base.OnAppearing();
		lstCitas.ItemsSource = null;
		lstCitas.ItemsSource = citasManager.ObtenerTodos;
	}

	private void lstCitas_ItemSelected(object sender, SelectedItemChangedEventArgs e)
	{
		Navigation.PushAsync(new EditarCitas(e.SelectedItem as Citas));

    }

	private void Button_Clicked(object sender, EventArgs e)
	{
		Navigation.PushAsync(new EditarCitas(new COMMON.Entidades.Citas()));
    }
}