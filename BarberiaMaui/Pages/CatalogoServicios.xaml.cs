using BIZ;
using BIZ.Managers;
using COMMON.Entidades;

namespace BarberiaMaui.Pages;

public partial class CatalogoServicios : ContentPage
{
	ServiciosManager serviciosManager;
	List<Servicios> servicios;
	public CatalogoServicios()
	{
		InitializeComponent();
		serviciosManager = FabricManager.ServiciosManager();

	}

	protected override void OnAppearing()
	{
		base.OnAppearing();
		lstServicios.ItemsSource = null;
		lstServicios.ItemsSource = serviciosManager.ObtenerTodos;
	}
	private void Button_Clicked_2(object sender, EventArgs e)
	{
		Navigation.PushAsync(new EditarServicios(new COMMON.Entidades.Servicios()));
	}

	private void lstServicios_ItemSelected(object sender, SelectedItemChangedEventArgs e)
	{
		Navigation.PushAsync(new EditarServicios(e.SelectedItem as Servicios));
	}
}